#include <iostream>
#include <cmath>
template<typename T>
class MyClass {
public:
	MyClass(int N, int M) : n(N), m(M) {
		mass = new T * [N];
		for (int i = 0; i < N; i++) {
			mass[i] = new T[M];
			for (int j = 0; j < M; j++)
				mass[i][j] = i + j;
		}
		x = new int(3);
		y = new char('1');
		z = new float(2);
	}
	MyClass& operator=(MyClass& other) {
		for (int i = 0; i < n; i++)
			delete[] mass[i];
		delete[] mass;
		n = other.n;
		m = other.m;
		mass = new T * [n];
		for (int i = 0; i < n; i++)
			mass[i] = new T[m];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				mass[i][j] = other.mass[i][j];
		x = new int(*(other.x));
		y = new char(*(other.y));
		z = new float(*(other.z));
		return *this;
	}
	~MyClass() {
		for (int i = 0; i < n; i++)
			delete[] mass[i];
		delete[] mass;
		delete x, y, z;
	}
private:
	int n, m;
	T** mass;
	int* x;
	char* y;
	float* z;
};

int main() {
	MyClass<int> c(3, 4);
	MyClass<int> d(5, 6);
	c = d;
	return 0;
}